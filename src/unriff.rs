//! Module for deserialization of RIFF metadata
//!
//! For an example of a program built on this library see [findcues.rs](https://gitlab.com/igorMavlevich/findcues.rs).
use riff::Chunk as ByteChunk;
use std::fmt;
use std::fs::File;
use tinystr::TinyStrError;
use tinystr::TinyStr4;

///Presents RIFF word (4-byte chunk) in a more convenient form
#[derive(Debug)]
pub enum Word {
    Number(u32),
    Text(TinyStr4),
}

impl fmt::Display for Word {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let out: String = match self {
            Self::Number(n) => n.to_string(),
            Self::Text(t) => t.to_string(),
        };
        write!(f, "{}", out)
    }
}

impl Word {
    fn deser_as_number(bytes: &[u8]) -> Self {
        let mut word: [u8; 4] = [0; 4];
        for i in 0..4 {
            word[i] = *bytes.get(i).expect("Word too short");
        }
        let n = u32::from_le_bytes(word);
        Self::Number(n)
    }
    /// # Errors
    ///
    /// Returns error if the bytes do not make a valid ASCII string
    ///
    /// # Examples
    ///
    /// ```
    /// let invalid_ascii: [u8] = [0; 4];
    /// assert_eq!(&invalid_ascii, TinyStrError);
    /// ```
    fn deser_as_text(bytes: &[u8]) -> Result<Self, TinyStrError> {
        Ok(Self::Text(TinyStr4::from_bytes(bytes)?))
    }

    pub fn as_number(&self) -> Option<&u32> {
        if let Self::Number(v) = self {
            Some(v)
        } else {
            None
        }
    }
}

///Presents generic RIFF chunk structure:
///
/// - id - name of the chunk;
/// - size - length of the chunk without id and size
/// - data - everything that goes after.
///
///Not intended for recursive chunks (like the main RIFF chunk)
#[derive(Debug)]
pub struct Chunk {
    pub id: TinyStr4,
    pub size: u32,
    pub data: Vec<Word>,
}

impl Chunk {
    /// Searches for requested chink and deserializes it.
    ///
    ///If the matrix is too short, it will be repeated; this allows decoding lists of similarly encoded structures.
    ///
    /// # Arguments
    ///
    /// - "search" is a 4-character, case-sensitive RIFF chunkId
    /// - deser_matrix marks how to decode each 4-byte word:
    ///   0 signifies a number, anything else means text.
    pub fn from_file(file: &mut File, search: &str, deser_matrix: &[usize]) -> Option<Chunk> {
        let metadata: ByteChunk = riff::Chunk::read(file, 0).expect("Failed to read file.");

        // Find the position for the requested chunk
        let mut some_pos = None;
        for child in metadata.iter(file) {
            if child.id().as_str() == search {
                some_pos = Some(child.offset())
            }
        }
        if some_pos.is_none() {
            return None;
        }
        let pos = some_pos.unwrap();

        // Read the requested chunk
        let raw_chunk: ByteChunk = ByteChunk::read(file, pos).expect("Failed to read file.");
        let id = TinyStr4::from_str(raw_chunk.id().as_str()).unwrap();

        let raw_data: Vec<u8> = raw_chunk.read_contents(file).expect("Failed to read file.");

        //Decode data from byte stream
        let data = Chunk::data_deser(&raw_data, deser_matrix);

        Some(Chunk {
            id,
            size: raw_chunk.len(),
            data,
        })
    }

    ///Deserializes byte stream according to given matrix.
    ///
    /// deser_matrix marks how to decode each 4-byte word:
    ///
    /// 0 signifies a number, anything else means text
    ///
    /// If the matrix is too short, it will be repeated; this allows decoding lists of similarly encoded structures.
    ///
    /// # Panics
    ///
    /// Will panic! if instructed to decode non-ascii word as text
    pub fn data_deser(bytes: &Vec<u8>, deser_matrix: &[usize]) -> Vec<Word> {
        let mat: Vec<usize> = deser_matrix.iter().rev().cloned().collect();
        //Preallocae some space for decoded data
        let mut data: Vec<Word> = Vec::with_capacity(1+bytes.len()/4);

        let len = deser_matrix.len();
        let mut i = 0;
        let mut chunk_iter = bytes.chunks(4);
        loop {
            let some_chunk = chunk_iter.next();

            let chunk = match some_chunk {
                Some(b) => b,
                None => break,
            };

            data.push(match mat[i] {
                0 => Word::deser_as_text(chunk).expect("Tried to decode non-ASCII word as text"),
                _ => Word::deser_as_number(chunk),
            });

            i += 1;
            if i == len {
                i = 0;
            }
        }
        data
    }
}
