use clap::Parser;
use std::{fmt, fs::File, io::Write, path::PathBuf, u32};

use libfindcues::{Chunk, Word};

#[derive(Parser)]
#[clap(version = "0.3.2", author = "Igor M.")]
struct Opts {
    input: Vec<PathBuf>,
    #[clap(short, long)]
    replace: bool,
    #[clap(short, long)]
    output: Option<PathBuf>,
}

#[derive(Debug)]
struct Timestamp {
    start: f64,
    end: f64,
    label: String,
}

impl fmt::Display for Timestamp {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{:.6}{}{:.6}{}{}",
            self.start, '\t', self.end, '\t', self.label
        )
    }
}

impl Timestamp {
    ///Calculates time in seconds
    fn from_offset(sample_offset: u32, sample_rate: u32) -> f64 {
        //let u_samples: u64 = 1_000_000 * (sample_offset as u64);
        sample_offset as f64 / (sample_rate as f64)
    }
}

fn main() {
    let opts = Opts::parse();

    for path in opts.input {
        println!("Reading file: {}", path.to_str().unwrap());
        extract_export(&path, &opts.output, opts.replace);
        println!("");
    }
}

fn extract_export(input: &PathBuf, output: &Option<PathBuf>, replace: bool) {
    if !check_input(input) {
        println!("Invalid name");
        return;
    }

    //Set valid output file name
    let output = match output {
        Some(s) => fix_input(s),
        None => input.with_extension("txt").clone(),
    };

    //Open input file
    let some_wav = File::open(input);
    if some_wav.is_err() {
        let kind = some_wav.unwrap_err().to_string();
        println!("Couldn't read file: {}", kind);
        return;
    }
    let mut wav = some_wav.expect("Failed to unwrap input file");

    //Gather data
    let some_fmt_chunk = Chunk::from_file(&mut wav, "fmt ", &[1]);
    if some_fmt_chunk.is_none() {
        println!("Missing fmt chunk");
        return;
    }
    let fmt_chunk = some_fmt_chunk.expect("Failed to unwrap fmt chunk");
    let sample_rate = *fmt_chunk.data[1]
        .as_number()
        .expect("Failed to parse sampling frequency");
    //Let's be nice and give the user some feedback
    println!("Sampling frequency: {} Hz", sample_rate);

    //Now time to look for cues
    let cue_matrix = [1, 1, 0, 1, 1, 1];
    let some_cue_chunk = Chunk::from_file(&mut wav, "cue ", &cue_matrix);
    if some_cue_chunk.is_none() {
        println!("File does not contain cues");
        return;
    }
    let cue_chunk = some_cue_chunk.expect("Failed to unwrap cue chunk");
    let cues: Vec<Word> = cue_chunk.data;

    println!("Found {} cue points", cues[0]);

    //Buffer for export data, with some preallocated space
    let mut buf: Vec<u8> = Vec::with_capacity(32); 

    //Each CuePoint uses 6 Words
    //So we move in steps of 6
    for cue_point in cues[1..].chunks(6) {
        let sample_offset = match cue_point[5] {
            Word::Number(n) => n,
            _ => 0,
        };
        let id: u32 = match cue_point[0] {
            Word::Number(n) => n,
            _ => 0,
        };

        let ts: Timestamp = Timestamp {
            start: Timestamp::from_offset(sample_offset, sample_rate) as f64,
            end: Timestamp::from_offset(sample_offset, sample_rate) as f64 + 1.0,
            label: format!("{} {}", "Mark", id),
        };

        for byte in ts.to_string().as_bytes() {
            buf.push(*byte);
        }
        buf.push(b'\n');
    }

    if output.exists() && !replace {
        println!("File {} already exists; abort", output.to_str().unwrap());
        return;
    }

    //Export data
    let mut label_txt: File =
        std::fs::File::create(output.clone()).expect("Failed to create file.");
    label_txt.write_all(&buf).expect("Export error");
    println!("Output written to {}", output.to_str().unwrap());
}

///Checks if input ends with ".wav" and is at least 5 characters long
fn check_input(path: &PathBuf) -> bool {
    let ext = match path.extension() {
        Some(e) => e.to_str().unwrap(),
        _ => return false,
    };
    if ext.eq_ignore_ascii_case("WAV") {
        return true;
    }
    false
}

///Makes sure that input ends with ".txt", adds extention if it doesn't
fn fix_input(path: &PathBuf) -> PathBuf {
    let ext = match path.extension() {
        Some(e) => e.to_str().unwrap(),
        None => "nop",
    };
    match ext {
        "TXT" | "txt" => return path.clone(),
        _ => return path.with_extension("txt"),
    }
}
