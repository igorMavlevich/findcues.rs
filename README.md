# findcues.rs
## Description
This is a simple program that reads cues or marks made by a digital recorder from a .WAV (BWF) file and exports them
as a .txt file for import into Audacity.

Vaguely inspired by the `findcues.py` script from [Audacity wiki](https://wiki.audacityteam.org/wiki/Importing_Timestamp_Information).

## Usage
`findcues filename.wav`

The program produces file named "filename.txt". Can accept multiple files: 

`findcues *.wav` `findcues file.wav anotherfile.wav`

Can output into arbitrary .txt file:

`findcues --output file.txt audio.wav`

Note that custom output cannot be used with multiple inputs.

The program will abort export if the output file already exists. To overwrite the output, one may use corresponding flag:

`findcues --replace file.wav`

See also: `findcues --help`.

## Disclamer
This program is provided as-is, use it at your own risk.
